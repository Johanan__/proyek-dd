﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyek_DD
{
    public partial class Form1 : Form
    {
        public login l;

        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            book b = new book();
            b.f1 = this;
            b.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            l.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            update_stat_lapangan usl = new update_stat_lapangan();
            usl.Show();
            usl.f1 = this;
            this.Hide();
        }
    }
}
