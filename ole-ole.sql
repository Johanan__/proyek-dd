--id_customer = 1901001 (<tahun 2 digit><bulan 2 digit><urutan 3 digit>)
create table customer(
	id_customer varchar2(7) primary key,
	nama_customer varchar2(50),
	contact_customer varchar2(12)
);
--id_lapangan = FF1 (<FF fantasy futsal atau OO ole-ole><nomer lapangan>)
create table lapangan(
	id_lapangan varchar2(3) primary key,
	status_lapangan varchar2(1)
);
--id_reservasi = 2019010101 (<tahun 4 digit><bulan 2 digit><tanggal 2 digit><urutan 2 digit>)
create table reservasi(
	id_reservasi varchar2(10) primary key,
	start_reservasi datetime,
	end_reservasi datetime,
	id_customer varchar2(7),
	id_lapangan varchar2(3),
	status_reservasi varchar2(1),
	constraint FK_CUSTOMER foreign key (id_customer) references customer(id_customer),
	constraint FK_LAPANGAN foreign key (id_lapangan) references lapangan(id_lapangan)
);